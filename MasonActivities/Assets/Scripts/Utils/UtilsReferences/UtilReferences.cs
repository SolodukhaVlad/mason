﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "ScriptableObjects/Containers/UtilReferences")]
public class UtilReferences : SingletonSO<UtilReferences>
{
    [SerializeField]
    private LoggerController loggerController;

    public static LoggerController LoggerController { get => Instance.loggerController; }
}
