﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface ILogger
{
    void MakeLog(string log);
}