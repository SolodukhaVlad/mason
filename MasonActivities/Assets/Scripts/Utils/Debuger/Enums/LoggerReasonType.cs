﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum LoggerReasonType
{
    LevelLoaded,
    LevelLoadedError,
    LevelUnloaded,
    LevelUnloadedError
}
