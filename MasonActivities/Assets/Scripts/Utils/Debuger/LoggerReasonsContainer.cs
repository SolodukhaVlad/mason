﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu( menuName = "ScriptableObjects/Containers/LoggerReasonsContainer")]
public class LoggerReasonsContainer : ScriptableObject
{
    [SerializeField]
    private List<LoggerReasonDescription> loggerReasonDescriptions = new List<LoggerReasonDescription>();

    private string _emptyString = string.Empty;

    public string GetDescriptionByType(LoggerReasonType reason)
    {
        if (loggerReasonDescriptions == null)
        {
            return _emptyString;
        }

        int index = loggerReasonDescriptions.FindIndex(reasonDescription => reasonDescription.reasonType == reason);
        return (index != -1) ? loggerReasonDescriptions[index].description : _emptyString;
    }
}
