﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "ScriptableObjects/Controllers/LoggerController")]
public class LoggerController : ScriptableObject
{
    [SerializeField]
    private LoggerReasonsContainer   debuggerReasonsContainer;
    private List<ILogger>            loggers;

    private void OnEnable()
    {
        InitLoggers();
    }

    private void InitLoggers()
    {
        loggers = new List<ILogger>();
        loggers.Add(new ConsoleLogger());
    }

    public void Log(LoggerReasonType reasonType)
    {
        string logText = debuggerReasonsContainer.GetDescriptionByType(reasonType);
        foreach (var logger in loggers)
        {
            logger.MakeLog(logText);
        }
    }

}
