﻿using System;

[Serializable]
public class LoggerReasonDescription
{
    public LoggerReasonType reasonType;
    public string             description;
}
