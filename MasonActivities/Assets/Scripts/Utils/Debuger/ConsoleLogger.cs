﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ConsoleLogger : ILogger
{
    public void MakeLog(string log)
    {
        Debug.Log(log);
    }
}
