﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SingletonSO<T> : ScriptableObject where T : SingletonSO<T>
{
    private static T instance;

    public static T Instance
    {
        get => instance;
    }

    public static bool IsInitialized
    {
        get => instance != null;
    }


    protected virtual void OnEnable()
    {
        if (instance == null)
        {
            instance = (T)this;
        }
        else
        {
            Debug.Log($"There are object with type {instance.GetType()} already created ");
        }
    }

    protected virtual void OnDestroy()
    {
        if (instance == this)
        {
            instance = null;
        }
    }

}
