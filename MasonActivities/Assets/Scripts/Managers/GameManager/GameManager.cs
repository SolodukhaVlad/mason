﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : SingletonMono<GameManager>
{
    private string                   levelName = string.Empty;

    public  GameObject[]             systemPrefabs;
    private List<GameObject>         instantiatedSystemPrefabsList;
    private List<AsyncOperation>     asyncOperations;

    private void Start()
    {
        DontDestroyOnLoad(this.gameObject);

        asyncOperations                 = new List<AsyncOperation>();
        instantiatedSystemPrefabsList   = new List<GameObject>();
        InstantiateSystemPrefabs();
        
        LoadLevel("Main");
    }

    protected override void OnDestroy()
    {
        base.OnDestroy();
        ClearSystemPrefabs();
    }

    private void LevelWasLoaded(AsyncOperation _asyncOperation)
    {
        if (asyncOperations.Contains(_asyncOperation))
        {
            asyncOperations.Remove(_asyncOperation);
        }
        UtilReferences.LoggerController.Log(LoggerReasonType.LevelLoaded);
    }

    private void LevelWasUnloaded(AsyncOperation _asyncOperation)
    {
        UtilReferences.LoggerController.Log(LoggerReasonType.LevelUnloaded);
    }

    private void InstantiateSystemPrefabs()
    {
        GameObject currentGameObject;
        for (int i = 0; i < systemPrefabs.Length; i++)
        {
            currentGameObject = Instantiate(systemPrefabs[i]);
            instantiatedSystemPrefabsList.Add(currentGameObject);
        }
    }

    private void ClearSystemPrefabs()
    {
        for (int i = 0; i < instantiatedSystemPrefabsList.Count; i++)
        {
            Destroy(instantiatedSystemPrefabsList[i]);
        }
        instantiatedSystemPrefabsList.Clear();
    }

    public void LoadLevel(string _levelName)
    {
        AsyncOperation loadResult   = SceneManager.LoadSceneAsync(_levelName, LoadSceneMode.Additive);
        if (loadResult == null)
        {
            UtilReferences.LoggerController.Log(LoggerReasonType.LevelLoadedError);
            return;
        }
        asyncOperations.Add(loadResult);
        loadResult.completed        += LevelWasLoaded;
        levelName                   = _levelName;
    }

    public void UnLoadLevel(string _levelName)
    {
        AsyncOperation loadResult   = SceneManager.UnloadSceneAsync(_levelName);

        if (loadResult == null)
        {
            UtilReferences.LoggerController.Log(LoggerReasonType.LevelUnloadedError);
            return;
        }

        loadResult.completed        += LevelWasUnloaded;
        levelName                   = _levelName;
    }
}
